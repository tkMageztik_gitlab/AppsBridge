﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace To.AppsBridge.Web.Models
{
    public class Application
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public string Path { set; get; }
    }
}
