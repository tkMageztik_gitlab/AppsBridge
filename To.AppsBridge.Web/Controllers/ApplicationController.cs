﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace To.AppsBridge.Web.Controllers
{
    public class ApplicationController : Controller
    {
        private string Cookie { set; get; }
        private string ResponseData { set; get; }
        private string RequestURL { set; get; }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Ultimatix()
        {
            //await LoginInterface("");

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            await LoginInterface();
            await LoginCC();
            //await CallBack();
            await GetApiKey();
            return View("Dashboard");
        }

        public async Task LoginInterface()
        {
            string query;
            var queryContent = new FormUrlEncodedContent(new KeyValuePair<string, string>[]{
                new KeyValuePair<string, string>("GUID", ""),
                new KeyValuePair<string, string>("METHOD", "GET"),
                new KeyValuePair<string, string>("REALMOID", "-SM-k3NCKvCkroes4pEwq0AQwup2pCbB6XRbHfApTf6UwdEA2+whWWoUw07LXypODrCb"),
                new KeyValuePair<string, string>("SMAGENTNAME", "-SM-k3NCKvCkroes4pEwq0AQwup2pCbB6XRbHfApTf6UwdEA2+whWWoUw07LXypODrCb"),
                new KeyValuePair<string, string>("TARGET", "-SM-HTTPS://authapp.ultimatix.net/ultimatix/redirect/redirect.jsp?SAMLRequest=fZLLTsMwFER-/JTuvEjcOhdZqKkVUSJUAIV4LNugmuREWjm18bR5-/j5MKAQvY2jNnRmNvCEbtZBPDk7nGl4gUsoYIfVDWnFpDcUR-/g-/5VdXh3fV6zpxAcSc6dho8cnCoGj9TZYkQek404JBSfqLwDrVvonlm2S1hlYGJ-+EyYhOFdEHdSY7t4Lg4HDMLxhS4dE4i62WnUzTxBZlu13NXscuvWiWp70eVuuq-/zoqF-/lq2MhclijEGXbgyiXSUoUcW8ogAk1E4tylS-+WeVXeCiGrSpbHDyy7T5XnVqJYsOx91IbkFFaz6I20QIqkgRFJhk7eNBfnMgklfC300-+L-+9zhvg-+2sZtvNpJZzO7-/9Z88N-/yncHF7qMoH3uyubVvnIGq3t26lHCFiz4COy7Mz6NObfVcqinE9Unw-+zVOIISjd9n3KJ8e0h9feX2H4C&SMPORTALURL=https-:-/-/authapp.ultimatix.net-/affwebservices-/public-/saml2sso"),
                new KeyValuePair<string, string>("TYPE", "33554432"),
            });
            query = queryContent.ReadAsStringAsync().Result;

            string baseUrl = "https://auth.ultimatix.net/", requestURL = "utxLogin/login";

            try
            {
                var cookieContainer = new CookieContainer();
                var handler = new HttpClientHandler() { CookieContainer = cookieContainer, /*Proxy = GetProxy()*/ };

                using (HttpClient client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Host = "auth.ultimatix.net";
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xhtml+xml"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*", 0.8));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("es-ES"));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("es", 0.8));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en-US", 0.5));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en", 0.3));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("br"));
                    client.DefaultRequestHeaders.Connection.Add("keep-alive");
                    //client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("C# app")));
                    client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");

                    RequestURL = requestURL + "?" + query;
                    using (HttpResponseMessage response = await client.GetAsync(RequestURL))
                    {
                        RequestURL = response.RequestMessage.RequestUri.ToString();
                        //Console.WriteLine(NextRequestURL);
                        using (HttpContent content = response.Content)
                        {
                            //responseData = await content.ReadAsStringAsync();
                            //TODO: almancenarlo en sesión o a nivel de claims de seguridad.
                            Cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }

        public async Task LoginCC()
        {
            var cookieParts = Cookie.Split(new[] { ';' });

            var queryContent = new FormUrlEncodedContent(new KeyValuePair<string, string>[]{
                new KeyValuePair<string, string>("SMENC", "UTF-8"),
                new KeyValuePair<string, string>("SMLOCALE", "US-EN"),
                //new KeyValuePair<string, string>("SMLOCALE", "ES-PE"),
                new KeyValuePair<string, string>("target", "-SM-https%3A%2F%2Fauthapp.ultimatix.net%2Fultimatix%2Fredirect%2Fredirect.jsp?SAMLRequest=fZLLTsMwFER-/JTuvEjcOhdZqKkVUSJUAIV4LNugmuREWjm18bR5-/j5MKAQvY2jNnRmNvCEbtZBPDk7nGl4gUsoYIfVDWnFpDcUR-/g-/5VdXh3fV6zpxAcSc6dho8cnCoGj9TZYkQek404JBSfqLwDrVvonlm2S1hlYGJ-+EyYhOFdEHdSY7t4Lg4HDMLxhS4dE4i62WnUzTxBZlu13NXscuvWiWp70eVuuq-/zoqF-/lq2MhclijEGXbgyiXSUoUcW8ogAk1E4tylS-+WeVXeCiGrSpbHDyy7T5XnVqJYsOx91IbkFFaz6I20QIqkgRFJhk7eNBfnMgklfC300-+L-+9zhvg-+2sZtvNpJZzO7-/9Z88N-/yncHF7qMoH3uyubVvnIGq3t26lHCFiz4COy7Mz6NObfVcqinE9Unw-+zVOIISjd9n3KJ8e0h9feX2H4C&SMPORTALURL=https-:-/-/authapp.ultimatix.net-/affwebservices-/public-/saml2sso"),
                new KeyValuePair<string, string>("USER", "tkmageztik"),
                new KeyValuePair<string, string>("PASSWORD", "Santiago230611@25"),
            });

            String baseUrl = "https://auth.ultimatix.net";
            String requestURL = "/ultimatixLogin/login.fcc";

            try
            {
                var cookieContainer = new CookieContainer();
                var handler = new HttpClientHandler() { CookieContainer = cookieContainer/*, Proxy = GetProxy()*/ };

                using (HttpClient client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Host = "auth.ultimatix.net";
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/html"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xhtml+xml"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*", 0.8));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("es-ES"));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("es", 0.8));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en-US", 0.5));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en", 0.3));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
                    client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("br"));
                    client.DefaultRequestHeaders.Connection.Add("keep-alive");
                    //client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("C# App")));
                    client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                    client.DefaultRequestHeaders.Add("Cookie", cookieParts[0]);
                    //client.DefaultRequestHeaders.Referrer = new Uri("https://auth.ultimatix.net/utxLogin/login?TYPE=33554432&REALMOID=06-9374027c-7828-4b8f-9228-94f04c85cf02&GUID=&SMAUTHREASON=0&METHOD=GET&SMAGENTNAME=-SM-k3NCKvCkroes4pEwq0AQwup2pCbB6XRbHfApTf6UwdEA2%2bwhWWoUw07LXypODrCb&TARGET=-SM-HTTPS:%2f%2fauthapp.ultimatix.net%2fultimatix%2fredirect%2fredirect.jsp%3fSAMLRequest%3dfZLLTsMwFER-%2FJTuvEjcOhdZqKkVUSJUAIV4LNugmuREWjm18bR5-%2Fj5MKAQvY2jNnRmNvCEbtZBPDk7nGl4gUsoYIfVDWnFpDcUR-%2Fg-%2F5VdXh3fV6zpxAcSc6dho8cnCoGj9TZYkQek404JBSfqLwDrVvonlm2S1hlYGJ-%2BEyYhOFdEHdSY7t4Lg4HDMLxhS4dE4i62WnUzTxBZlu13NXscuvWiWp70eVuuq-%2FzoqF-%2Flq2MhclijEGXbgyiXSUoUcW8ogAk1E4tylS-%2BWeVXeCiGrSpbHDyy7T5XnVqJYsOx91IbkFFaz6I20QIqkgRFJhk7eNBfnMgklfC300-%2BL-%2B9zhvg-%2B2sZtvNpJZzO7-%2F9Z88N-%2FyncHF7qMoH3uyubVvnIGq3t26lHCFiz4COy7Mz6NObfVcqinE9Unw-%2BzVOIISjd9n3KJ8e0h9feX2H4C%26SMPORTALURL%3dhttps-:-%2F-%2Fauthapp.ultimatix.net-%2Faffwebservices-%2Fpublic-%2Fsaml2sso");
                    client.DefaultRequestHeaders.Referrer = new Uri(RequestURL);

                    using (HttpResponseMessage response = await client.PostAsync(requestURL, queryContent))
                    {
                        RequestURL = response.RequestMessage.RequestUri.ToString();
                        //Console.WriteLine(NextRequestURL);
                        using (HttpContent content = response.Content)
                        {
                            ResponseData = await content.ReadAsStringAsync();
                            Cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex) { }

            await CallBack(ResponseData);
        }

        public String getSAMLResponse(String responseData)
        {
            String newString = responseData.Substring(responseData.LastIndexOf("value"));
            String newString2 = newString.Substring(7);
            String newString3 = newString.Substring(newString.IndexOf(">"));
            String finalString = newString2.Substring(0, newString2.IndexOf(">") - 1);

            //return "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIERl c3RpbmF0aW9uPSJodHRwczovL3BsYXktYXBpLmZyZXNjby5tZS91c2Vycy9hdXRoL3NhbWwvY2Fs bGJhY2siIElEPSJfNzBjNDk4YTQ5YzIwNTczYWEwMWViY2M2ZDM1YmNlNTc5ZTc3IiBJblJlc3Bv bnNlVG89Il9mYzkwMzU3ZC1iMTkzLTQ0ZDgtODYyMi1hOWUyMjFiZGEyMTUiIElzc3VlSW5zdGFu dD0iMjAxOC0wNi0wMVQyMTowOTowNVoiIFZlcnNpb249IjIuMCI+CiAgICA8bnMxOklzc3VlciB4 bWxuczpuczE9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIEZvcm1hdD0i dXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOm5hbWVpZC1mb3JtYXQ6ZW50aXR5Ij5odHRwczov L3d3dy51bHRpbWF0aXgubmV0PC9uczE6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0 dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbz4KPGRzOkNh bm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEw L3htbC1leGMtYzE0biMiLz4KPGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93 d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz4KPGRzOlJlZmVyZW5j ZSBVUkk9IiNfNzBjNDk4YTQ5YzIwNTczYWEwMWViY2M2ZDM1YmNlNTc5ZTc3Ij4KPGRzOlRyYW5z Zm9ybXM+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5 L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPgo8ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0i aHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+CjwvZHM6VHJhbnNmb3Jt cz4KPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQv eG1sZW5jI3NoYTI1NiIvPgo8ZHM6RGlnZXN0VmFsdWU+OWJ4d1NuYXZKNm5JNjdsRGp2Vkl4UTBM U1Bsc3hvTDBuc2RyeTVNcnlmVT08L2RzOkRpZ2VzdFZhbHVlPgo8L2RzOlJlZmVyZW5jZT4KPC9k czpTaWduZWRJbmZvPgo8ZHM6U2lnbmF0dXJlVmFsdWU+CmNXZVpvMXRxRG5WdlRFc2Uyck5JZnF3 RzhVM0QzRkVlZldqUVR2dHdkUDFFNDVOTzhaeXpUMVZqQWNUaHFhNkh4NEtRZ2VvY09Pek0Kdmx1 eUhBdFQ2RzJlNDd3NTJablVaM1VJQjhoa3VMVFpCM0dwaStZaVhJQWIzamNNMjM3NSs1OU5zaExk Z0dwWHkyai8yWXJMM3lNVQpNTnJVS29FcTdyNHJUMkhtV0RDTlBqTkJxN1ppeUFERDczNVJhRG05 WWFMazlsRjNzTnJ5dWJPQjJqc0gyYmZqUk5ETi9mVkdOdnF0CkNiREp3Y05hd09sMUoxY2o4amcw OFlHbmxrQ01Cdis2WXU5MDQ2UW9OTWJsOGlsdlJ6ZFVrbHU3bXgrd0tveTVnNGdqUDVlVkZwRzYK RC9MVnA3YUxrUjZpU0ZEb1FxclFqVXNDbDFhR1VCV0cwQjBrQzRQMVpzNTB0VFJhY0gzMnE5NlZ6 ZTgzaEtYdy90YWJ6dzlQcmtPaQpPTmRoWktUSGJqbVgxemp1OXpjb0hDdUFvZ1dxVmNRcUVORUVt d25lRlk5ellzVXI3VWdBSGoxL1BRdGRjZnNLMVNHdUJELy84R2xSCkRJNFNGengxZkxzS1p2dWY4 d3NkM0JxQSs3VlZWM2I1UzFyYjNqZW5sMHArZHpHVlFIdFpKcThJZ3JkY1NoSDlwMlJkWHp4RnNX cU0Kc3Yyd3ZzY0djb0pTY3ZmWEw4ZXdhM0ZXWGJhMlZYL3pVY2g1NGltMzJoWE5BZVQ5T2NING5S czFueHROVnpibUpTcU55YXZPbUVDQgo5akV2NzNmbVVZZmVqUEJrYmkyNWxrcGJHbnVubWNTZEU0 VDdlZVlSOXdGSHZsaTVLWTdpc3hhL1E1TDMzaGI3M3BlOFZCWmE1YlE9CjwvZHM6U2lnbmF0dXJl VmFsdWU+CjxkczpLZXlJbmZvPgo8ZHM6WDUwOURhdGE+CjxkczpYNTA5Q2VydGlmaWNhdGU+Ck1J SUY5VENDQk4yZ0F3SUJBZ0lDRUhvd0RRWUpLb1pJaHZjTkFRRUxCUUF3Z1pNeEN6QUpCZ05WQkFZ VEFrbE9NUlF3RWdZRFZRUUkKRXd0TllXaGhjbUZ6YUhSeVlURU1NQW9HQTFVRUNoTURWRU5UTVJJ d0VBWURWUVFMRXdsVmJIUnBiV0YwYVhneEVEQU9CZ05WQkFNVApCMVJEVXlCRFNVOHhLVEFuQmdr cWhraUc5dzBCQ1FFV0dsVnNkR2x0WVhScGVDNUlaV3h3WkdWemEwQjBZM011WTI5dE1ROHdEUVlE ClZRUUhFd1pOZFcxaVlXa3dIaGNOTVRReE1USTRNVEV5T1RNd1doY05NalF4TVRJMU1URXlPVE13 V2pDQmt6RU1NQW9HQTFVRUF4TUQKVlVGTk1SUXdFZ1lEVlFRTEV3dEpiblJsY201aGJDQkpWREVN TUFvR0ExVUVDaE1EVkVOVE1ROHdEUVlEVlFRSEV3Wk5kVzFpWVdreApGREFTQmdOVkJBZ1RDMDFo YUdGeVlYTm9kSEpoTVFzd0NRWURWUVFHRXdKSlRqRXJNQ2tHQ1NxR1NJYjNEUUVKQVJZY2RXeDBh VzFoCmRHbDRjM052TG5OMWNIQnZjblJBZEdOekxtTnZiVENDQWlJd0RRWUpLb1pJaHZjTkFRRUJC UUFEZ2dJUEFEQ0NBZ29DZ2dJQkFOOHEKVWdxTHNSUzI0ZGlTU0pXZ2U1WkR2amFObE9KZmZLOHdt Sk5pZVBad3lCY0xTUDJaTkcweGZMV2JJQlFIVjhjT2w3OU9VaDNMUmdtdAp5ZzFSdVhTY0dsTmZu L2VocENKcDU3emJGTkNjRlgrYlFpZjBoTmRYS083UEZZaXN4RTl1VHk1Ym51eW5hU2o4OWMzeDhG V1dscUZ1ClJ4TFM2NmRHVk44OEpSbnM2MVV2WUl3NGIzQld2WUxETkxsUmRWUk5LT0NuVGpaR09F RVlkZTgyR3JXdlBETU9zUnRNdUxWMys4OHAKYVA4N3F0WFdEckV3NlU1VFlGWFE2V3NNSXJnY09j NnJqK2IzSVlvc3pKeHV6N0lhSTRqalZZcSswbjFNMHhaT3NTL3NPcWxlakluOQpOTTFjNWZjRDFw OWpMZFQyY1lKOEd3TzVPZDRmYXJpNDhld1pwMW9BbHVVeVlLTlZCUVVqQ041L3lWbG9zQ0JVVGs4 MFZoeHk1bmMxClFDNnlrYWZsVUpUUUYrUWtXcmJwYXg1Y3J4c1RhN0Z0RVNQWXZFcVU1RS9nRS9W eEFnaWVhQ3BkQnN0MTMxWjZiUGo4YW9vbUZ0bFcKanFlaWJldTNiMDdRNE14VEJjek9SRG9kcFNv dFJBbUF5YmlFdmJiUE50a2tXUE5HMm9FT09GVDhhc1JTY1NTVzFCb01rTGl3ek10Uwp2bmxhdHNO cVFkYTRHS2dQdjZOQ01CcVpIK0hLZXg3MFZ5ZEE2aTVRYWRzb1QwTWs4UGxxTEFlSlI1YWhiaUIr QWpaLzJicmExYWRKCjVPTWVaRXJqZFk3cGk5amROU2Vpa1VmeHhMQXpkOFF4KytaSnhoYWFCajZk ZVBxUTdjaGV1akMxcGY4WGIrMndNNnlITDI5SEFnTUIKQUFHamdnRlBNSUlCU3pBSkJnTlZIUk1F QWpBQU1CMEdBMVVkRGdRV0JCU09kQ1lzb3ZsNjNFZTFwQ0txeitqUUhNbDJpakNCeUFZRApWUjBq QklIQU1JRzlnQlNoYk51TE5aWUxNWmc4eFNFeXdCVWRZMGZxM3FHQm1hU0JsakNCa3pFTE1Ba0dB MVVFQmhNQ1NVNHhGREFTCkJnTlZCQWdUQzAxaGFHRnlZWE5vZEhKaE1Rd3dDZ1lEVlFRS0V3TlVR MU14RWpBUUJnTlZCQXNUQ1ZWc2RHbHRZWFJwZURFUU1BNEcKQTFVRUF4TUhWRU5USUVOSlR6RXBN Q2NHQ1NxR1NJYjNEUUVKQVJZYVZXeDBhVzFoZEdsNExraGxiSEJrWlhOclFIUmpjeTVqYjIweApE ekFOQmdOVkJBY1RCazExYldKaGFZSUpBTXU5T2UxdDlHTFZNRFFHQTFVZEpRUXRNQ3NHQ1dDR1NB R0crRUlFQVFZS0t3WUJCQUdDCk53b0RBd1lJS3dZQkJRVUhBd0VHQ0NzR0FRVUZCd01DTUFzR0Ex VWREd1FFQXdJRm9EQVJCZ2xnaGtnQmh2aENBUUVFQkFNQ0JzQXcKRFFZSktvWklodmNOQVFFTEJR QURnZ0VCQURrM0dJNTNxWG1ZVTBsWHB0b3BVdEE0cUZNQXd1NlZwWVVrVjlKTHUyWExZckdmVjl3 aQpmcHlaU1hXcGdhemIrdVFYM1lXL2FRWXZWRXNCT1pPdEMzYk5nakdCdnlhd1lRWkdvaGtsNlpC V2l3VzByQWs5U2Njd2xFbTBXUURMCmVVVnQzUUduZFFLLzZ0R25RMk5YQVdJOW5PT2RWWEpETjAr dlphOVYyK2ljTnZLeDBhN3NhVFFnVXZnSlJMNWQrVGN2VnFheVR2RmIKTGJIWExTSzB6TFYzVUcw eC9OTjFPYmFUL0U0ZXVOUU5jeTE3dFJLRjRZUllkSHB4cmh6ZEsvR3hYUUZoMjhhSi9uQkpIUU55 SHR1UApkQUl6cURnNUs3cnRzZy9aTkFmRXlTdWFZcFUvT2VrWURFcjRKTlVaZ2hhYjJxdWxhNXhu a01JMlhZcENOSjg9CjwvZHM6WDUwOUNlcnRpZmljYXRlPgo8L2RzOlg1MDlEYXRhPgo8L2RzOktl eUluZm8+CjwvZHM6U2lnbmF0dXJlPgogICAgPFN0YXR1cz4KICAgICAgICA8U3RhdHVzQ29kZSBW YWx1ZT0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnN0YXR1czpTdWNjZXNzIi8+CiAgICA8 L1N0YXR1cz4KICAgIDxuczI6QXNzZXJ0aW9uIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRj OlNBTUw6Mi4wOmFzc2VydGlvbiIgSUQ9Il9mNTg1ODE4MGI0N2I0Y2M2ZmNjMDA0ZGJlMGExNmMx OWQ3YTQiIElzc3VlSW5zdGFudD0iMjAxOC0wNi0wMVQyMTowOTowNVoiIFZlcnNpb249IjIuMCI+ CiAgICAgICAgPG5zMjpJc3N1ZXIgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6 bmFtZWlkLWZvcm1hdDplbnRpdHkiPmh0dHBzOi8vd3d3LnVsdGltYXRpeC5uZXQ8L25zMjpJc3N1 ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxk c2lnIyI+CjxkczpTaWduZWRJbmZvPgo8ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0 aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPgo8ZHM6U2lnbmF0 dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1v cmUjcnNhLXNoYTI1NiIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI19mNTg1ODE4MGI0N2I0Y2M2ZmNj MDA0ZGJlMGExNmMxOWQ3YTQiPgo8ZHM6VHJhbnNmb3Jtcz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0 aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJl Ii8+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3ht bC1leGMtYzE0biMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRo bT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8+CjxkczpEaWdlc3RW YWx1ZT5DNWhzN1EybUxwT1BVc1JZQkVYT3JIYmNuci92Z3RWMTZJTTVGN3pOYmc4PTwvZHM6RGln ZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVW YWx1ZT4KQVRlNHExOE9nbGsvY0FsbDhhTW9DeHpoWTcwZURhK1BJTzB2Z0dhMTlWV1VuMHdsTlFn UmMySUEzWUQwTGkzeEJPMWVCSzYvelRlMwo5UnB2cjMwcDZPZ2dueGthaEZDdnUrLzEzZ09INys0 UjY5NGpFU1V2aWlhWksyVVVWbmo2eWltR0dQZVdSWWVuc1JTclZrYTN3U2p5CmNmZ01PR3M5K2lh RmVHWTV3bEFFUEhaQUZYOFE5cjU3aU92OHJXbDhTVWplcDl4MG9OSDdOVUxnemVnTzIzeWNuWTlI cXRpNk1aVEgKRnRSa2lKdkRYQ1Q2MXJiR0V1T1ZVMllBOXBiY1VHS29Ec3lnT0Jwb2RzaTUwQW1U M3laeEpFdVk3NWRMRlIrRTdHRjUvMUNQZlpCVQpwTnNNaVU1Um1mOE54WTRVVzM4MEJCR1g3RjlE MGZoSFViemlpS3JNYUNpRzFvbG1SZTloTFRDbW0yeExwTHJwVy9oNE1MMUhRS1hoCjdWVGhacml6 c1VNdjF6MVNnTVliMzIxdnlkNG4xVzlRWVFxRHpjOTVla3JzMGhkM2lORms0YUZpamdMRTZlWGlJ KytBWjRRK2c1c2EKT0t3SDRhUHRFdmUxTzhlWmRaUDlIeTZ5SjFieE50blF6UmVEbG9sZS8xZi8x a3AvN1BuZUxHYU9sZjBYdHpnSVdoSjM2Qms0S1NnWQp6TTFFdmlPQkh5UnJaUUdMRThhSEdGbHhj clNaY01YTU8xOG1oakw2OWIwUy9hRWpkL2FpOFlVUC9LcUtQSFBOeDZsZjArUEkwWXNOCkh1cnJW c1BoUmRrTlN1YzRGR2d4dlpHenFWQmNiaEhZNnErWHVDeTNFY0VwcWs0bWdhMnQ0eW4xenBJdXkz bXhXa1NaQTQ2bWN2Zz0KPC9kczpTaWduYXR1cmVWYWx1ZT4KPGRzOktleUluZm8+CjxkczpYNTA5 RGF0YT4KPGRzOlg1MDlDZXJ0aWZpY2F0ZT4KTUlJRjlUQ0NCTjJnQXdJQkFnSUNFSG93RFFZSktv WklodmNOQVFFTEJRQXdnWk14Q3pBSkJnTlZCQVlUQWtsT01SUXdFZ1lEVlFRSQpFd3ROWVdoaGNt RnphSFJ5WVRFTU1Bb0dBMVVFQ2hNRFZFTlRNUkl3RUFZRFZRUUxFd2xWYkhScGJXRjBhWGd4RURB T0JnTlZCQU1UCkIxUkRVeUJEU1U4eEtUQW5CZ2txaGtpRzl3MEJDUUVXR2xWc2RHbHRZWFJwZUM1 SVpXeHdaR1Z6YTBCMFkzTXVZMjl0TVE4d0RRWUQKVlFRSEV3Wk5kVzFpWVdrd0hoY05NVFF4TVRJ NE1URXlPVE13V2hjTk1qUXhNVEkxTVRFeU9UTXdXakNCa3pFTU1Bb0dBMVVFQXhNRApWVUZOTVJR d0VnWURWUVFMRXd0SmJuUmxjbTVoYkNCSlZERU1NQW9HQTFVRUNoTURWRU5UTVE4d0RRWURWUVFI RXdaTmRXMWlZV2t4CkZEQVNCZ05WQkFnVEMwMWhhR0Z5WVhOb2RISmhNUXN3Q1FZRFZRUUdFd0pK VGpFck1Da0dDU3FHU0liM0RRRUpBUlljZFd4MGFXMWgKZEdsNGMzTnZMbk4xY0hCdmNuUkFkR056 TG1OdmJUQ0NBaUl3RFFZSktvWklodmNOQVFFQkJRQURnZ0lQQURDQ0Fnb0NnZ0lCQU44cQpVZ3FM c1JTMjRkaVNTSldnZTVaRHZqYU5sT0pmZks4d21KTmllUFp3eUJjTFNQMlpORzB4ZkxXYklCUUhW OGNPbDc5T1VoM0xSZ210CnlnMVJ1WFNjR2xOZm4vZWhwQ0pwNTd6YkZOQ2NGWCtiUWlmMGhOZFhL TzdQRllpc3hFOXVUeTVibnV5bmFTajg5YzN4OEZXV2xxRnUKUnhMUzY2ZEdWTjg4SlJuczYxVXZZ SXc0YjNCV3ZZTEROTGxSZFZSTktPQ25UalpHT0VFWWRlODJHcld2UERNT3NSdE11TFYzKzg4cAph UDg3cXRYV0RyRXc2VTVUWUZYUTZXc01JcmdjT2M2cmorYjNJWW9zekp4dXo3SWFJNGpqVllxKzBu MU0weFpPc1Mvc09xbGVqSW45Ck5NMWM1ZmNEMXA5akxkVDJjWUo4R3dPNU9kNGZhcmk0OGV3WnAx b0FsdVV5WUtOVkJRVWpDTjUveVZsb3NDQlVUazgwVmh4eTVuYzEKUUM2eWthZmxVSlRRRitRa1dy YnBheDVjcnhzVGE3RnRFU1BZdkVxVTVFL2dFL1Z4QWdpZWFDcGRCc3QxMzFaNmJQajhhb29tRnRs VwpqcWVpYmV1M2IwN1E0TXhUQmN6T1JEb2RwU290UkFtQXliaUV2YmJQTnRra1dQTkcyb0VPT0ZU OGFzUlNjU1NXMUJvTWtMaXd6TXRTCnZubGF0c05xUWRhNEdLZ1B2Nk5DTUJxWkgrSEtleDcwVnlk QTZpNVFhZHNvVDBNazhQbHFMQWVKUjVhaGJpQitBalovMmJyYTFhZEoKNU9NZVpFcmpkWTdwaTlq ZE5TZWlrVWZ4eExBemQ4UXgrK1pKeGhhYUJqNmRlUHFRN2NoZXVqQzFwZjhYYisyd002eUhMMjlI QWdNQgpBQUdqZ2dGUE1JSUJTekFKQmdOVkhSTUVBakFBTUIwR0ExVWREZ1FXQkJTT2RDWXNvdmw2 M0VlMXBDS3F6K2pRSE1sMmlqQ0J5QVlEClZSMGpCSUhBTUlHOWdCU2hiTnVMTlpZTE1aZzh4U0V5 d0JVZFkwZnEzcUdCbWFTQmxqQ0JrekVMTUFrR0ExVUVCaE1DU1U0eEZEQVMKQmdOVkJBZ1RDMDFo YUdGeVlYTm9kSEpoTVF3d0NnWURWUVFLRXdOVVExTXhFakFRQmdOVkJBc1RDVlZzZEdsdFlYUnBl REVRTUE0RwpBMVVFQXhNSFZFTlRJRU5KVHpFcE1DY0dDU3FHU0liM0RRRUpBUllhVld4MGFXMWhk R2w0TGtobGJIQmtaWE5yUUhSamN5NWpiMjB4CkR6QU5CZ05WQkFjVEJrMTFiV0poYVlJSkFNdTlP ZTF0OUdMVk1EUUdBMVVkSlFRdE1Dc0dDV0NHU0FHRytFSUVBUVlLS3dZQkJBR0MKTndvREF3WUlL d1lCQlFVSEF3RUdDQ3NHQVFVRkJ3TUNNQXNHQTFVZER3UUVBd0lGb0RBUkJnbGdoa2dCaHZoQ0FR RUVCQU1DQnNBdwpEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRGszR0k1M3FYbVlVMGxYcHRvcFV0 QTRxRk1Bd3U2VnBZVWtWOUpMdTJYTFlyR2ZWOXdpCmZweVpTWFdwZ2F6Yit1UVgzWVcvYVFZdlZF c0JPWk90QzNiTmdqR0J2eWF3WVFaR29oa2w2WkJXaXdXMHJBazlTY2N3bEVtMFdRREwKZVVWdDNRR25kUUsvNnRHblEyTlhBV0k5bk9PZFZYSkROMCt2WmE5VjIraWNOdkt4MGE3c2FUUWdVdmdKUkw1 ZCtUY3ZWcWF5VHZGYgpMYkhYTFNLMHpMVjNVRzB4L05OMU9iYVQvRTRldU5RTmN5MTd0UktGNFlS WWRIcHhyaHpkSy9HeFhRRmgyOGFKL25CSkhRTnlIdHVQCmRBSXpxRGc1SzdydHNnL1pOQWZFeVN1 YVlwVS9PZWtZREVyNEpOVVpnaGFiMnF1bGE1eG5rTUkyWFlwQ05KOD0KPC9kczpYNTA5Q2VydGlm aWNhdGU+CjwvZHM6WDUwOURhdGE+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+CiAgICAg ICAgPG5zMjpTdWJqZWN0PgogICAgICAgICAgICA8bnMyOk5hbWVJRCBGb3JtYXQ9InVybjpvYXNp czpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OmVtYWlsQWRkcmVzcyI+MTM1OTY5Nzwv bnMyOk5hbWVJRD4KICAgICAgICAgICAgPG5zMjpTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0i dXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI+CiAgICAgICAgICAgICAgICA8 bnMyOlN1YmplY3RDb25maXJtYXRpb25EYXRhIEluUmVzcG9uc2VUbz0iX2ZjOTAzNTdkLWIxOTMt NDRkOC04NjIyLWE5ZTIyMWJkYTIxNSIgTm90T25PckFmdGVyPSIyMDE4LTA2LTAxVDIxOjE1OjA1 WiIgUmVjaXBpZW50PSJodHRwczovL3BsYXktYXBpLmZyZXNjby5tZS91c2Vycy9hdXRoL3NhbWwv Y2FsbGJhY2siLz4KICAgICAgICAgICAgPC9uczI6U3ViamVjdENvbmZpcm1hdGlvbj4KICAgICAg ICA8L25zMjpTdWJqZWN0PgogICAgICAgIDxuczI6Q29uZGl0aW9ucyBOb3RCZWZvcmU9IjIwMTgt MDYtMDFUMjE6MDg6MDVaIiBOb3RPbk9yQWZ0ZXI9IjIwMTgtMDYtMDFUMjE6MTU6MDVaIj4KICAg ICAgICAgICAgPG5zMjpBdWRpZW5jZVJlc3RyaWN0aW9uPgogICAgICAgICAgICAgICAgPG5zMjpB dWRpZW5jZT5odHRwczovL3BsYXktYXBpLmZyZXNjby5tZS88L25zMjpBdWRpZW5jZT4KICAgICAg ICAgICAgPC9uczI6QXVkaWVuY2VSZXN0cmljdGlvbj4KICAgICAgICA8L25zMjpDb25kaXRpb25z PgogICAgICAgIDxuczI6QXV0aG5TdGF0ZW1lbnQgQXV0aG5JbnN0YW50PSIyMDE4LTA2LTAxVDIx OjA5OjA0WiIgU2Vzc2lvbkluZGV4PSI0ZVRNcTJNRzlVNmNHdkkvbmt2Uk94djVEUzQ9VUZyaXVB PT0iIFNlc3Npb25Ob3RPbk9yQWZ0ZXI9IjIwMTgtMDYtMDFUMjE6MTU6MDVaIj4KICAgICAgICAg ICAgPG5zMjpBdXRobkNvbnRleHQ+CiAgICAgICAgICAgICAgICA8bnMyOkF1dGhuQ29udGV4dENs YXNzUmVmPnVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkPC9u czI6QXV0aG5Db250ZXh0Q2xhc3NSZWY+CiAgICAgICAgICAgIDwvbnMyOkF1dGhuQ29udGV4dD4K ICAgICAgICA8L25zMjpBdXRoblN0YXRlbWVudD4KICAgICAgICA8bnMyOkF0dHJpYnV0ZVN0YXRl bWVudD4KICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0iZW1haWwiIE5hbWVGb3JtYXQ9 InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6YmFzaWMiPgogICAg ICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGVWYWx1ZT5tYXJjby5jYWJyZWpvc0B0Y3MuY29tPC9u czI6QXR0cmlidXRlVmFsdWU+CiAgICAgICAgICAgIDwvbnMyOkF0dHJpYnV0ZT4KICAgICAgICAg ICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0ibGFzdF9uYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6 bmFtZXM6dGM6U0FNTDoyLjA6YXR0cm5hbWUtZm9ybWF0OmJhc2ljIj4KICAgICAgICAgICAgICAg IDxuczI6QXR0cmlidXRlVmFsdWU+Q2FicmVqb3MgTWFsbWE8L25zMjpBdHRyaWJ1dGVWYWx1ZT4K ICAgICAgICAgICAgPC9uczI6QXR0cmlidXRlPgogICAgICAgICAgICA8bnMyOkF0dHJpYnV0ZSBO YW1lPSJmaXJzdF9uYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6 YXR0cm5hbWUtZm9ybWF0OmJhc2ljIj4KICAgICAgICAgICAgICAgIDxuczI6QXR0cmlidXRlVmFs dWU+TWFyY29hbnRvbmlvPC9uczI6QXR0cmlidXRlVmFsdWU+CiAgICAgICAgICAgIDwvbnMyOkF0 dHJpYnV0ZT4KICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0iZW1wbG95ZWVfaWQiIE5h bWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6YmFz aWMiPgogICAgICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGVWYWx1ZT4xMzU5Njk3PC9uczI6QXR0 cmlidXRlVmFsdWU+CiAgICAgICAgICAgIDwvbnMyOkF0dHJpYnV0ZT4KICAgICAgICA8L25zMjpB dHRyaWJ1dGVTdGF0ZW1lbnQ+CiAgICA8L25zMjpBc3NlcnRpb24+CjwvUmVzcG9uc2U+";
            //return "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIERl\r\nc3RpbmF0aW9uPSJodHRwczovL3BsYXktYXBpLmZyZXNjby5tZS91c2Vycy9hdXRoL3NhbWwvY2Fs\r\nbGJhY2siIElEPSJfMDVlMzg0MjMwMTI3YmQwNjJlYjVjMjM5MmVhYzg2N2M3ZWVlIiBJblJlc3Bv\r\nbnNlVG89Il9mYzkwMzU3ZC1iMTkzLTQ0ZDgtODYyMi1hOWUyMjFiZGEyMTUiIElzc3VlSW5zdGFu\r\ndD0iMjAxOC0wNi0wMVQyMjoyMjoyM1oiIFZlcnNpb249IjIuMCI+CiAgICA8bnMxOklzc3VlciB4\r\nbWxuczpuczE9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIEZvcm1hdD0i\r\ndXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOm5hbWVpZC1mb3JtYXQ6ZW50aXR5Ij5odHRwczov\r\nL3d3dy51bHRpbWF0aXgubmV0PC9uczE6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0\r\ndHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbz4KPGRzOkNh\r\nbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEw\r\nL3htbC1leGMtYzE0biMiLz4KPGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93\r\nd3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz4KPGRzOlJlZmVyZW5j\r\nZSBVUkk9IiNfMDVlMzg0MjMwMTI3YmQwNjJlYjVjMjM5MmVhYzg2N2M3ZWVlIj4KPGRzOlRyYW5z\r\nZm9ybXM+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5\r\nL3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPgo8ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0i\r\naHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+CjwvZHM6VHJhbnNmb3Jt\r\ncz4KPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQv\r\neG1sZW5jI3NoYTI1NiIvPgo8ZHM6RGlnZXN0VmFsdWU+SFROYjB2ZGdtMnF1RHhKR05rc1hNeGhY\r\nVG9lcklFb0VKZElhdGc1RWthaz08L2RzOkRpZ2VzdFZhbHVlPgo8L2RzOlJlZmVyZW5jZT4KPC9k\r\nczpTaWduZWRJbmZvPgo8ZHM6U2lnbmF0dXJlVmFsdWU+CkhJMFBza1R1dGNDOWowcTd1YXhqQ2k0\r\nVi9DQ1hPZDVtQlM5bGpUT1Vva281cDQyUDhhdlI2bGk2T2UxNHg2QXRZQWNxMlBqV1E1R2QKOEUw\r\nT0NMc00vdUdSWXJMeUxibDNtMVVmSzZpeUR1ZUZLQkdnQnYwTnVvaXIzRFMzT20rdUltREdJaFhS\r\nSzA0aUo1UUFES2NhWlc0SgpjZTNPSE5qeXNpVGpQR0VtZzZDdjdqdE9RMVE2RmJ5U1ZyQ2cxZWlq\r\nVlNyL3JDYit1aXZYZS9kdE04YlZHYXlwci9lRTVKOHNuSUQzCjAwUnRoUnpxMWlpdFNpWmphbk95\r\nandlZ1VEWVpqKytLRXREVzJxd2QxQVB1aHRweWs2WXJFdm5adXcvVFVnU1FPSzZaQkhiVEEyREEK\r\nNVA1S0FTeDZiZGplQW1WL1NlZ1hVbWpFSG5xNHB4elF2UThKaEx6U052MGR1eDltNWNnZ2RWUHo3\r\nVm9MRFUwbThkcHRiMDFCK21hegp5cnFnQnZzdFF6ZFBKbHJsWm41eGp6NHF4SmdPSUJQcEZBVTc2\r\nSlBtTkRqYmhtYmgrK052ZW51SHMzQUlJN2hKZ1NqN3NSWTF3YmZ4CkFuZ2JqZHBCb2hDQmJmSEhj\r\nRmVJTWFMeDRDb2M3b1RrTTQzREFXNUpPT0VCZi9MbnJSNTJ6Q2NiQS9IeHRBaUVaN3JtaGhQNktp\r\nTWUKaDNPcGxzczhrSm5zcjRnZkJxZTNvTzFuTDZzQjBrVzBUcGJtRUVwWm51U0FRWkt1K1lzYWU2\r\nV3VHN3JsK01sUXR3WmsvMVVrdkl3eAo2REpLR0NSd1dLSjRkTGRmMlA2WW1jYUJ0c2tTRHF5MFpM\r\nb1JkdnBnUU5GcW1LeVpNdDBEQ0FqVEN6dktBOWpXUXcwTVhCT2lzSEU9CjwvZHM6U2lnbmF0dXJl\r\nVmFsdWU+CjxkczpLZXlJbmZvPgo8ZHM6WDUwOURhdGE+CjxkczpYNTA5Q2VydGlmaWNhdGU+Ck1J\r\nSUY5VENDQk4yZ0F3SUJBZ0lDRUhvd0RRWUpLb1pJaHZjTkFRRUxCUUF3Z1pNeEN6QUpCZ05WQkFZ\r\nVEFrbE9NUlF3RWdZRFZRUUkKRXd0TllXaGhjbUZ6YUhSeVlURU1NQW9HQTFVRUNoTURWRU5UTVJJ\r\nd0VBWURWUVFMRXdsVmJIUnBiV0YwYVhneEVEQU9CZ05WQkFNVApCMVJEVXlCRFNVOHhLVEFuQmdr\r\ncWhraUc5dzBCQ1FFV0dsVnNkR2x0WVhScGVDNUlaV3h3WkdWemEwQjBZM011WTI5dE1ROHdEUVlE\r\nClZRUUhFd1pOZFcxaVlXa3dIaGNOTVRReE1USTRNVEV5T1RNd1doY05NalF4TVRJMU1URXlPVE13\r\nV2pDQmt6RU1NQW9HQTFVRUF4TUQKVlVGTk1SUXdFZ1lEVlFRTEV3dEpiblJsY201aGJDQkpWREVN\r\nTUFvR0ExVUVDaE1EVkVOVE1ROHdEUVlEVlFRSEV3Wk5kVzFpWVdreApGREFTQmdOVkJBZ1RDMDFo\r\nYUdGeVlYTm9kSEpoTVFzd0NRWURWUVFHRXdKSlRqRXJNQ2tHQ1NxR1NJYjNEUUVKQVJZY2RXeDBh\r\nVzFoCmRHbDRjM052TG5OMWNIQnZjblJBZEdOekxtTnZiVENDQWlJd0RRWUpLb1pJaHZjTkFRRUJC\r\nUUFEZ2dJUEFEQ0NBZ29DZ2dJQkFOOHEKVWdxTHNSUzI0ZGlTU0pXZ2U1WkR2amFObE9KZmZLOHdt\r\nSk5pZVBad3lCY0xTUDJaTkcweGZMV2JJQlFIVjhjT2w3OU9VaDNMUmdtdAp5ZzFSdVhTY0dsTmZu\r\nL2VocENKcDU3emJGTkNjRlgrYlFpZjBoTmRYS083UEZZaXN4RTl1VHk1Ym51eW5hU2o4OWMzeDhG\r\nV1dscUZ1ClJ4TFM2NmRHVk44OEpSbnM2MVV2WUl3NGIzQld2WUxETkxsUmRWUk5LT0NuVGpaR09F\r\nRVlkZTgyR3JXdlBETU9zUnRNdUxWMys4OHAKYVA4N3F0WFdEckV3NlU1VFlGWFE2V3NNSXJnY09j\r\nNnJqK2IzSVlvc3pKeHV6N0lhSTRqalZZcSswbjFNMHhaT3NTL3NPcWxlakluOQpOTTFjNWZjRDFw\r\nOWpMZFQyY1lKOEd3TzVPZDRmYXJpNDhld1pwMW9BbHVVeVlLTlZCUVVqQ041L3lWbG9zQ0JVVGs4\r\nMFZoeHk1bmMxClFDNnlrYWZsVUpUUUYrUWtXcmJwYXg1Y3J4c1RhN0Z0RVNQWXZFcVU1RS9nRS9W\r\neEFnaWVhQ3BkQnN0MTMxWjZiUGo4YW9vbUZ0bFcKanFlaWJldTNiMDdRNE14VEJjek9SRG9kcFNv\r\ndFJBbUF5YmlFdmJiUE50a2tXUE5HMm9FT09GVDhhc1JTY1NTVzFCb01rTGl3ek10Uwp2bmxhdHNO\r\ncVFkYTRHS2dQdjZOQ01CcVpIK0hLZXg3MFZ5ZEE2aTVRYWRzb1QwTWs4UGxxTEFlSlI1YWhiaUIr\r\nQWpaLzJicmExYWRKCjVPTWVaRXJqZFk3cGk5amROU2Vpa1VmeHhMQXpkOFF4KytaSnhoYWFCajZk\r\nZVBxUTdjaGV1akMxcGY4WGIrMndNNnlITDI5SEFnTUIKQUFHamdnRlBNSUlCU3pBSkJnTlZIUk1F\r\nQWpBQU1CMEdBMVVkRGdRV0JCU09kQ1lzb3ZsNjNFZTFwQ0txeitqUUhNbDJpakNCeUFZRApWUjBq\r\nQklIQU1JRzlnQlNoYk51TE5aWUxNWmc4eFNFeXdCVWRZMGZxM3FHQm1hU0JsakNCa3pFTE1Ba0dB\r\nMVVFQmhNQ1NVNHhGREFTCkJnTlZCQWdUQzAxaGFHRnlZWE5vZEhKaE1Rd3dDZ1lEVlFRS0V3TlVR\r\nMU14RWpBUUJnTlZCQXNUQ1ZWc2RHbHRZWFJwZURFUU1BNEcKQTFVRUF4TUhWRU5USUVOSlR6RXBN\r\nQ2NHQ1NxR1NJYjNEUUVKQVJZYVZXeDBhVzFoZEdsNExraGxiSEJrWlhOclFIUmpjeTVqYjIweApE\r\nekFOQmdOVkJBY1RCazExYldKaGFZSUpBTXU5T2UxdDlHTFZNRFFHQTFVZEpRUXRNQ3NHQ1dDR1NB\r\nR0crRUlFQVFZS0t3WUJCQUdDCk53b0RBd1lJS3dZQkJRVUhBd0VHQ0NzR0FRVUZCd01DTUFzR0Ex\r\nVWREd1FFQXdJRm9EQVJCZ2xnaGtnQmh2aENBUUVFQkFNQ0JzQXcKRFFZSktvWklodmNOQVFFTEJR\r\nQURnZ0VCQURrM0dJNTNxWG1ZVTBsWHB0b3BVdEE0cUZNQXd1NlZwWVVrVjlKTHUyWExZckdmVjl3\r\naQpmcHlaU1hXcGdhemIrdVFYM1lXL2FRWXZWRXNCT1pPdEMzYk5nakdCdnlhd1lRWkdvaGtsNlpC\r\nV2l3VzByQWs5U2Njd2xFbTBXUURMCmVVVnQzUUduZFFLLzZ0R25RMk5YQVdJOW5PT2RWWEpETjAr\r\ndlphOVYyK2ljTnZLeDBhN3NhVFFnVXZnSlJMNWQrVGN2VnFheVR2RmIKTGJIWExTSzB6TFYzVUcw\r\neC9OTjFPYmFUL0U0ZXVOUU5jeTE3dFJLRjRZUllkSHB4cmh6ZEsvR3hYUUZoMjhhSi9uQkpIUU55\r\nSHR1UApkQUl6cURnNUs3cnRzZy9aTkFmRXlTdWFZcFUvT2VrWURFcjRKTlVaZ2hhYjJxdWxhNXhu\r\na01JMlhZcENOSjg9CjwvZHM6WDUwOUNlcnRpZmljYXRlPgo8L2RzOlg1MDlEYXRhPgo8L2RzOktl\r\neUluZm8+CjwvZHM6U2lnbmF0dXJlPgogICAgPFN0YXR1cz4KICAgICAgICA8U3RhdHVzQ29kZSBW\r\nYWx1ZT0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnN0YXR1czpTdWNjZXNzIi8+CiAgICA8\r\nL1N0YXR1cz4KICAgIDxuczI6QXNzZXJ0aW9uIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRj\r\nOlNBTUw6Mi4wOmFzc2VydGlvbiIgSUQ9Il8xNjQxNGE3MGQxYWEzZTkyZmU5YzcxMDA4Mjg1Yjg5\r\nMjkxMWUiIElzc3VlSW5zdGFudD0iMjAxOC0wNi0wMVQyMjoyMjoyM1oiIFZlcnNpb249IjIuMCI+\r\nCiAgICAgICAgPG5zMjpJc3N1ZXIgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6\r\nbmFtZWlkLWZvcm1hdDplbnRpdHkiPmh0dHBzOi8vd3d3LnVsdGltYXRpeC5uZXQ8L25zMjpJc3N1\r\nZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxk\r\nc2lnIyI+CjxkczpTaWduZWRJbmZvPgo8ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0\r\naG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPgo8ZHM6U2lnbmF0\r\ndXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1v\r\ncmUjcnNhLXNoYTI1NiIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI18xNjQxNGE3MGQxYWEzZTkyZmU5\r\nYzcxMDA4Mjg1Yjg5MjkxMWUiPgo8ZHM6VHJhbnNmb3Jtcz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0\r\naG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJl\r\nIi8+CjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3ht\r\nbC1leGMtYzE0biMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRo\r\nbT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8+CjxkczpEaWdlc3RW\r\nYWx1ZT5zb055RmFpeUNwMy9YWXNrenpNbnMwVG95YlBQOXRtZVpFUU5zN0hzR2VvPTwvZHM6RGln\r\nZXN0VmFsdWU+CjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8+CjxkczpTaWduYXR1cmVW\r\nYWx1ZT4KVDdaOVljOU9zN0JVbCtOMlhPYlRZZDM3Ujh0dzR5bWZiSW9JMWtNbEQxVzJraGNFYlph\r\nbGFSSGMwM3FqTkdlWWhnZjhYNE8zbjZJSApxcWJjMnJsaTRoZEo4VXVEeTh2a2MzQms1amxidjBE\r\nYVhZVm5ZZEJuOG1wWHdSZ1Y0NkFFM1FHYW1EU1pBSFFsRE1BcGROc050aUpuCiswM3ZQcjdCYWh2\r\nYlBqVmovVkorNFk5TkxGMmZsNzlTMGNTb3FVSm5ldnVTdWRMVGVaaTRSZ3ZRb3dnLzFDTTkyUG8v\r\neisyQVBNanQKWFN0dS8ybkZTc0V6bFVpeU44UFBaUEt3YWFPTnZkelVITUxsWTBOUmJPSG1ZNlIv\r\nV3pTNHZRNzdRMHlyNHJBZzByMGhBN2JNOWhMVApHMmJqazYxbE5qbVVSdHdiMGYvQUNOT1NYK1I2\r\ndU9LVjgyc2UwZ1huUVFQQjZMRXpwNGQ5c0F4alZUOG5TN3JOdU5GL1dTZksxRFRRCjlNN3loS3Vq\r\nNEVLSXdsYktBTG9zSitndGFnaU1CMU5Sa1A4ME4vTEllb2Q4UlFRMmxkV1dPZjNsRG8wakFzMVNv\r\nUSt2dGZSSHM2eW8KcHY5WG5RVjNWaEczaFFobUtCUVJZSFlxU2d2ZDdSV01DQ2c5UUtYdGJKdGNQ\r\nOG8rcWdsc2J0WFh2a0NWV3JxMVhiZ1RoTU9VTEFZdQo0U1FXdmtCYlQ3b1BsZEIzUnluTEtFQlcw\r\nVUMzbXVWeU9jbXVwSXBvZ2x1b01tSTRhNXJPRnJIY3hsNlNZSDdyT0RGdCtreGpsSEpTCjVYLzFo\r\nZzlaMEZ0cUVCcEQ4YlFIMFBMVVE4aFg3Tml2TlFoSi83ZnRncStGcG5iYmlKTWdvcHFiV090Z1pL\r\nZ2cyWmVzQmt2clJSTT0KPC9kczpTaWduYXR1cmVWYWx1ZT4KPGRzOktleUluZm8+CjxkczpYNTA5\r\nRGF0YT4KPGRzOlg1MDlDZXJ0aWZpY2F0ZT4KTUlJRjlUQ0NCTjJnQXdJQkFnSUNFSG93RFFZSktv\r\nWklodmNOQVFFTEJRQXdnWk14Q3pBSkJnTlZCQVlUQWtsT01SUXdFZ1lEVlFRSQpFd3ROWVdoaGNt\r\nRnphSFJ5WVRFTU1Bb0dBMVVFQ2hNRFZFTlRNUkl3RUFZRFZRUUxFd2xWYkhScGJXRjBhWGd4RURB\r\nT0JnTlZCQU1UCkIxUkRVeUJEU1U4eEtUQW5CZ2txaGtpRzl3MEJDUUVXR2xWc2RHbHRZWFJwZUM1\r\nSVpXeHdaR1Z6YTBCMFkzTXVZMjl0TVE4d0RRWUQKVlFRSEV3Wk5kVzFpWVdrd0hoY05NVFF4TVRJ\r\nNE1URXlPVE13V2hjTk1qUXhNVEkxTVRFeU9UTXdXakNCa3pFTU1Bb0dBMVVFQXhNRApWVUZOTVJR\r\nd0VnWURWUVFMRXd0SmJuUmxjbTVoYkNCSlZERU1NQW9HQTFVRUNoTURWRU5UTVE4d0RRWURWUVFI\r\nRXdaTmRXMWlZV2t4CkZEQVNCZ05WQkFnVEMwMWhhR0Z5WVhOb2RISmhNUXN3Q1FZRFZRUUdFd0pK\r\nVGpFck1Da0dDU3FHU0liM0RRRUpBUlljZFd4MGFXMWgKZEdsNGMzTnZMbk4xY0hCdmNuUkFkR056\r\nTG1OdmJUQ0NBaUl3RFFZSktvWklodmNOQVFFQkJRQURnZ0lQQURDQ0Fnb0NnZ0lCQU44cQpVZ3FM\r\nc1JTMjRkaVNTSldnZTVaRHZqYU5sT0pmZks4d21KTmllUFp3eUJjTFNQMlpORzB4ZkxXYklCUUhW\r\nOGNPbDc5T1VoM0xSZ210CnlnMVJ1WFNjR2xOZm4vZWhwQ0pwNTd6YkZOQ2NGWCtiUWlmMGhOZFhL\r\nTzdQRllpc3hFOXVUeTVibnV5bmFTajg5YzN4OEZXV2xxRnUKUnhMUzY2ZEdWTjg4SlJuczYxVXZZ\r\nSXc0YjNCV3ZZTEROTGxSZFZSTktPQ25UalpHT0VFWWRlODJHcld2UERNT3NSdE11TFYzKzg4cAph\r\nUDg3cXRYV0RyRXc2VTVUWUZYUTZXc01JcmdjT2M2cmorYjNJWW9zekp4dXo3SWFJNGpqVllxKzBu\r\nMU0weFpPc1Mvc09xbGVqSW45Ck5NMWM1ZmNEMXA5akxkVDJjWUo4R3dPNU9kNGZhcmk0OGV3WnAx\r\nb0FsdVV5WUtOVkJRVWpDTjUveVZsb3NDQlVUazgwVmh4eTVuYzEKUUM2eWthZmxVSlRRRitRa1dy\r\nYnBheDVjcnhzVGE3RnRFU1BZdkVxVTVFL2dFL1Z4QWdpZWFDcGRCc3QxMzFaNmJQajhhb29tRnRs\r\nVwpqcWVpYmV1M2IwN1E0TXhUQmN6T1JEb2RwU290UkFtQXliaUV2YmJQTnRra1dQTkcyb0VPT0ZU\r\nOGFzUlNjU1NXMUJvTWtMaXd6TXRTCnZubGF0c05xUWRhNEdLZ1B2Nk5DTUJxWkgrSEtleDcwVnlk\r\nQTZpNVFhZHNvVDBNazhQbHFMQWVKUjVhaGJpQitBalovMmJyYTFhZEoKNU9NZVpFcmpkWTdwaTlq\r\nZE5TZWlrVWZ4eExBemQ4UXgrK1pKeGhhYUJqNmRlUHFRN2NoZXVqQzFwZjhYYisyd002eUhMMjlI\r\nQWdNQgpBQUdqZ2dGUE1JSUJTekFKQmdOVkhSTUVBakFBTUIwR0ExVWREZ1FXQkJTT2RDWXNvdmw2\r\nM0VlMXBDS3F6K2pRSE1sMmlqQ0J5QVlEClZSMGpCSUhBTUlHOWdCU2hiTnVMTlpZTE1aZzh4U0V5\r\nd0JVZFkwZnEzcUdCbWFTQmxqQ0JrekVMTUFrR0ExVUVCaE1DU1U0eEZEQVMKQmdOVkJBZ1RDMDFo\r\nYUdGeVlYTm9kSEpoTVF3d0NnWURWUVFLRXdOVVExTXhFakFRQmdOVkJBc1RDVlZzZEdsdFlYUnBl\r\nREVRTUE0RwpBMVVFQXhNSFZFTlRJRU5KVHpFcE1DY0dDU3FHU0liM0RRRUpBUllhVld4MGFXMWhk\r\nR2w0TGtobGJIQmtaWE5yUUhSamN5NWpiMjB4CkR6QU5CZ05WQkFjVEJrMTFiV0poYVlJSkFNdTlP\r\nZTF0OUdMVk1EUUdBMVVkSlFRdE1Dc0dDV0NHU0FHRytFSUVBUVlLS3dZQkJBR0MKTndvREF3WUlL\r\nd1lCQlFVSEF3RUdDQ3NHQVFVRkJ3TUNNQXNHQTFVZER3UUVBd0lGb0RBUkJnbGdoa2dCaHZoQ0FR\r\nRUVCQU1DQnNBdwpEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRGszR0k1M3FYbVlVMGxYcHRvcFV0\r\nQTRxRk1Bd3U2VnBZVWtWOUpMdTJYTFlyR2ZWOXdpCmZweVpTWFdwZ2F6Yit1UVgzWVcvYVFZdlZF\r\nc0JPWk90QzNiTmdqR0J2eWF3WVFaR29oa2w2WkJXaXdXMHJBazlTY2N3bEVtMFdRREwKZVVWdDNR\r\nR25kUUsvNnRHblEyTlhBV0k5bk9PZFZYSkROMCt2WmE5VjIraWNOdkt4MGE3c2FUUWdVdmdKUkw1\r\nZCtUY3ZWcWF5VHZGYgpMYkhYTFNLMHpMVjNVRzB4L05OMU9iYVQvRTRldU5RTmN5MTd0UktGNFlS\r\nWWRIcHhyaHpkSy9HeFhRRmgyOGFKL25CSkhRTnlIdHVQCmRBSXpxRGc1SzdydHNnL1pOQWZFeVN1\r\nYVlwVS9PZWtZREVyNEpOVVpnaGFiMnF1bGE1eG5rTUkyWFlwQ05KOD0KPC9kczpYNTA5Q2VydGlm\r\naWNhdGU+CjwvZHM6WDUwOURhdGE+CjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU+CiAgICAg\r\nICAgPG5zMjpTdWJqZWN0PgogICAgICAgICAgICA8bnMyOk5hbWVJRCBGb3JtYXQ9InVybjpvYXNp\r\nczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OmVtYWlsQWRkcmVzcyI+MTM1OTY5Nzwv\r\nbnMyOk5hbWVJRD4KICAgICAgICAgICAgPG5zMjpTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0i\r\ndXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI+CiAgICAgICAgICAgICAgICA8\r\nbnMyOlN1YmplY3RDb25maXJtYXRpb25EYXRhIEluUmVzcG9uc2VUbz0iX2ZjOTAzNTdkLWIxOTMt\r\nNDRkOC04NjIyLWE5ZTIyMWJkYTIxNSIgTm90T25PckFmdGVyPSIyMDE4LTA2LTAxVDIyOjI4OjIz\r\nWiIgUmVjaXBpZW50PSJodHRwczovL3BsYXktYXBpLmZyZXNjby5tZS91c2Vycy9hdXRoL3NhbWwv\r\nY2FsbGJhY2siLz4KICAgICAgICAgICAgPC9uczI6U3ViamVjdENvbmZpcm1hdGlvbj4KICAgICAg\r\nICA8L25zMjpTdWJqZWN0PgogICAgICAgIDxuczI6Q29uZGl0aW9ucyBOb3RCZWZvcmU9IjIwMTgt\r\nMDYtMDFUMjI6MjE6MjNaIiBOb3RPbk9yQWZ0ZXI9IjIwMTgtMDYtMDFUMjI6Mjg6MjNaIj4KICAg\r\nICAgICAgICAgPG5zMjpBdWRpZW5jZVJlc3RyaWN0aW9uPgogICAgICAgICAgICAgICAgPG5zMjpB\r\ndWRpZW5jZT5odHRwczovL3BsYXktYXBpLmZyZXNjby5tZS88L25zMjpBdWRpZW5jZT4KICAgICAg\r\nICAgICAgPC9uczI6QXVkaWVuY2VSZXN0cmljdGlvbj4KICAgICAgICA8L25zMjpDb25kaXRpb25z\r\nPgogICAgICAgIDxuczI6QXV0aG5TdGF0ZW1lbnQgQXV0aG5JbnN0YW50PSIyMDE4LTA2LTAxVDIy\r\nOjIyOjIzWiIgU2Vzc2lvbkluZGV4PSJ2dThFblV0bUlvYjNDbFJScWlpTjlmdVo5WEk9REE2czdB\r\nPT0iIFNlc3Npb25Ob3RPbk9yQWZ0ZXI9IjIwMTgtMDYtMDFUMjI6Mjg6MjNaIj4KICAgICAgICAg\r\nICAgPG5zMjpBdXRobkNvbnRleHQ+CiAgICAgICAgICAgICAgICA8bnMyOkF1dGhuQ29udGV4dENs\r\nYXNzUmVmPnVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkPC9u\r\nczI6QXV0aG5Db250ZXh0Q2xhc3NSZWY+CiAgICAgICAgICAgIDwvbnMyOkF1dGhuQ29udGV4dD4K\r\nICAgICAgICA8L25zMjpBdXRoblN0YXRlbWVudD4KICAgICAgICA8bnMyOkF0dHJpYnV0ZVN0YXRl\r\nbWVudD4KICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0iZW1haWwiIE5hbWVGb3JtYXQ9\r\nInVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6YmFzaWMiPgogICAg\r\nICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGVWYWx1ZT5tYXJjby5jYWJyZWpvc0B0Y3MuY29tPC9u\r\nczI6QXR0cmlidXRlVmFsdWU+CiAgICAgICAgICAgIDwvbnMyOkF0dHJpYnV0ZT4KICAgICAgICAg\r\nICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0ibGFzdF9uYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6\r\nbmFtZXM6dGM6U0FNTDoyLjA6YXR0cm5hbWUtZm9ybWF0OmJhc2ljIj4KICAgICAgICAgICAgICAg\r\nIDxuczI6QXR0cmlidXRlVmFsdWU+Q2FicmVqb3MgTWFsbWE8L25zMjpBdHRyaWJ1dGVWYWx1ZT4K\r\nICAgICAgICAgICAgPC9uczI6QXR0cmlidXRlPgogICAgICAgICAgICA8bnMyOkF0dHJpYnV0ZSBO\r\nYW1lPSJmaXJzdF9uYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6\r\nYXR0cm5hbWUtZm9ybWF0OmJhc2ljIj4KICAgICAgICAgICAgICAgIDxuczI6QXR0cmlidXRlVmFs\r\ndWU+TWFyY29hbnRvbmlvPC9uczI6QXR0cmlidXRlVmFsdWU+CiAgICAgICAgICAgIDwvbnMyOkF0\r\ndHJpYnV0ZT4KICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0iZW1wbG95ZWVfaWQiIE5h\r\nbWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6YmFz\r\naWMiPgogICAgICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGVWYWx1ZT4xMzU5Njk3PC9uczI6QXR0\r\ncmlidXRlVmFsdWU+CiAgICAgICAgICAgIDwvbnMyOkF0dHJpYnV0ZT4KICAgICAgICA8L25zMjpB\r\ndHRyaWJ1dGVTdGF0ZW1lbnQ+CiAgICA8L25zMjpBc3NlcnRpb24+CjwvUmVzcG9uc2U+";
            return finalString;
        }

        public async Task CallBack2()
        {
            var queryContent = new FormUrlEncodedContent(new KeyValuePair<string, string>[]{
                new KeyValuePair<string, string>("SAMLResponse",getSAMLResponse(ResponseData))
            });

            String baseUrl = "https://play-api.fresco.me/";
            String requestUrl = "users/auth/saml/callback";

            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);

            try
            {
                var cookieContainer = new CookieContainer();
                var handler = new HttpClientHandler() { CookieContainer = cookieContainer/*, Proxy = GetProxy()*/ };

                using (HttpClient client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    client.DefaultRequestHeaders.Host = "play-api.fresco.me";
                    client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                    client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
                    client.DefaultRequestHeaders.Add("Accept-Language", "es-ES,es;q=0.9,en;q=0.8");
                    client.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                    client.DefaultRequestHeaders.Add("Origin", "https://authapp.ultimatix.net");
                    client.DefaultRequestHeaders.Connection.Add("keep-alive");
                    client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                    client.DefaultRequestHeaders.Add("Referer", RequestURL);
                    client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");

                    using (HttpResponseMessage response = await client.PostAsync(requestUrl, queryContent))
                    {
                        RequestURL = response.RequestMessage.RequestUri.ToString();
                        //Console.WriteLine(NextRequestURL);
                        using (HttpContent content = response.Content)
                        {
                            Cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }

        ////return responseData;
        //HttpWebRequest GETRequest = (HttpWebRequest)WebRequest.Create("https://play-api.fresco.me/users/auth/saml");
        //GETRequest.Method = "GET";
            
        //    //GETRequest.Accept = "application/json";
        //    ////GETRequest.Connection = "keep-alive";
        //    //GETRequest.Host = "play-api.fresco.me";
        //    //GETRequest.Referer = "https://play.fresco.me/profile";
        //    //GETRequest.Headers.Add("X-Api-Key", "F5MxkvSiCIzcMfvv1NAoxTdtXqDA9_SEoLV0fd6b5Nw");
        //    //F5MxkvSiCIzcMfvv1NAoxTdtXqDA9_SEoLV0fd6b5Nw
        //    StreamReader responseReader = new StreamReader(GETRequest.GetResponse().GetResponseStream());

        //string responseData = responseReader.ReadToEnd();

        //responseReader.Close();

        //    //employee_t_factor
        //    //HttpWebRequest GETRequest = (HttpWebRequest)WebRequest.Create("https://play-api.fresco.me/api/v1/users/profile.json");
        //    HttpWebRequest GETRequest2 = (HttpWebRequest)WebRequest.Create("https://play-api.fresco.me/api/v1/users/full_profile.json");
        //GETRequest.Method = "GET";
        //    GETRequest.Accept = "application/json";
        //    //GETRequest.Connection = "keep-alive";
        //    GETRequest.Host = "play-api.fresco.me";
        //    GETRequest.Referer = "https://play.fresco.me/profile";
        //    GETRequest.Headers.Add("X-Api-Key", "F5MxkvSiCIzcMfvv1NAoxTdtXqDA9_SEoLV0fd6b5Nw");
        //    //F5MxkvSiCIzcMfvv1NAoxTdtXqDA9_SEoLV0fd6b5Nw
        //    StreamReader responseReader2 = new StreamReader(GETRequest.GetResponse().GetResponseStream());

        //string responseData2 = responseReader.ReadToEnd();

        //responseReader.Close();

        //    HttpClient client = new HttpClient();

        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public async Task CallBack(string responseData)
        {
            String baseUrl = "https://play-api.fresco.me/";
            String requestUrl = "users/auth/saml/callback";
            String SAMLResponse = "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIERl%0D%0Ac3RpbmF0aW9uPSJodHRwczovL3BsYXktYXBpLmZyZXNjby5tZS91c2Vycy9hdXRoL3NhbWwvY2Fs%0D%0AbGJhY2siIElEPSJfNzBjNDk4YTQ5YzIwNTczYWEwMWViY2M2ZDM1YmNlNTc5ZTc3IiBJblJlc3Bv%0D%0AbnNlVG89Il9mYzkwMzU3ZC1iMTkzLTQ0ZDgtODYyMi1hOWUyMjFiZGEyMTUiIElzc3VlSW5zdGFu%0D%0AdD0iMjAxOC0wNi0wMVQyMTowOTowNVoiIFZlcnNpb249IjIuMCI%2BCiAgICA8bnMxOklzc3VlciB4%0D%0AbWxuczpuczE9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIEZvcm1hdD0i%0D%0AdXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOm5hbWVpZC1mb3JtYXQ6ZW50aXR5Ij5odHRwczov%0D%0AL3d3dy51bHRpbWF0aXgubmV0PC9uczE6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0%0D%0AdHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPgo8ZHM6U2lnbmVkSW5mbz4KPGRzOkNh%0D%0Abm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEw%0D%0AL3htbC1leGMtYzE0biMiLz4KPGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93%0D%0Ad3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz4KPGRzOlJlZmVyZW5j%0D%0AZSBVUkk9IiNfNzBjNDk4YTQ5YzIwNTczYWEwMWViY2M2ZDM1YmNlNTc5ZTc3Ij4KPGRzOlRyYW5z%0D%0AZm9ybXM%2BCjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5%0D%0AL3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPgo8ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0i%0D%0AaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8%2BCjwvZHM6VHJhbnNmb3Jt%0D%0Acz4KPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQv%0D%0AeG1sZW5jI3NoYTI1NiIvPgo8ZHM6RGlnZXN0VmFsdWU%2BOWJ4d1NuYXZKNm5JNjdsRGp2Vkl4UTBM%0D%0AU1Bsc3hvTDBuc2RyeTVNcnlmVT08L2RzOkRpZ2VzdFZhbHVlPgo8L2RzOlJlZmVyZW5jZT4KPC9k%0D%0AczpTaWduZWRJbmZvPgo8ZHM6U2lnbmF0dXJlVmFsdWU%2BCmNXZVpvMXRxRG5WdlRFc2Uyck5JZnF3%0D%0ARzhVM0QzRkVlZldqUVR2dHdkUDFFNDVOTzhaeXpUMVZqQWNUaHFhNkh4NEtRZ2VvY09Pek0Kdmx1%0D%0AeUhBdFQ2RzJlNDd3NTJablVaM1VJQjhoa3VMVFpCM0dwaStZaVhJQWIzamNNMjM3NSs1OU5zaExk%0D%0AZ0dwWHkyai8yWXJMM3lNVQpNTnJVS29FcTdyNHJUMkhtV0RDTlBqTkJxN1ppeUFERDczNVJhRG05%0D%0AWWFMazlsRjNzTnJ5dWJPQjJqc0gyYmZqUk5ETi9mVkdOdnF0CkNiREp3Y05hd09sMUoxY2o4amcw%0D%0AOFlHbmxrQ01Cdis2WXU5MDQ2UW9OTWJsOGlsdlJ6ZFVrbHU3bXgrd0tveTVnNGdqUDVlVkZwRzYK%0D%0ARC9MVnA3YUxrUjZpU0ZEb1FxclFqVXNDbDFhR1VCV0cwQjBrQzRQMVpzNTB0VFJhY0gzMnE5NlZ6%0D%0AZTgzaEtYdy90YWJ6dzlQcmtPaQpPTmRoWktUSGJqbVgxemp1OXpjb0hDdUFvZ1dxVmNRcUVORUVt%0D%0Ad25lRlk5ellzVXI3VWdBSGoxL1BRdGRjZnNLMVNHdUJELy84R2xSCkRJNFNGengxZkxzS1p2dWY4%0D%0Ad3NkM0JxQSs3VlZWM2I1UzFyYjNqZW5sMHArZHpHVlFIdFpKcThJZ3JkY1NoSDlwMlJkWHp4RnNX%0D%0AcU0Kc3Yyd3ZzY0djb0pTY3ZmWEw4ZXdhM0ZXWGJhMlZYL3pVY2g1NGltMzJoWE5BZVQ5T2NING5S%0D%0AczFueHROVnpibUpTcU55YXZPbUVDQgo5akV2NzNmbVVZZmVqUEJrYmkyNWxrcGJHbnVubWNTZEU0%0D%0AVDdlZVlSOXdGSHZsaTVLWTdpc3hhL1E1TDMzaGI3M3BlOFZCWmE1YlE9CjwvZHM6U2lnbmF0dXJl%0D%0AVmFsdWU%2BCjxkczpLZXlJbmZvPgo8ZHM6WDUwOURhdGE%2BCjxkczpYNTA5Q2VydGlmaWNhdGU%2BCk1J%0D%0ASUY5VENDQk4yZ0F3SUJBZ0lDRUhvd0RRWUpLb1pJaHZjTkFRRUxCUUF3Z1pNeEN6QUpCZ05WQkFZ%0D%0AVEFrbE9NUlF3RWdZRFZRUUkKRXd0TllXaGhjbUZ6YUhSeVlURU1NQW9HQTFVRUNoTURWRU5UTVJJ%0D%0Ad0VBWURWUVFMRXdsVmJIUnBiV0YwYVhneEVEQU9CZ05WQkFNVApCMVJEVXlCRFNVOHhLVEFuQmdr%0D%0AcWhraUc5dzBCQ1FFV0dsVnNkR2x0WVhScGVDNUlaV3h3WkdWemEwQjBZM011WTI5dE1ROHdEUVlE%0D%0AClZRUUhFd1pOZFcxaVlXa3dIaGNOTVRReE1USTRNVEV5T1RNd1doY05NalF4TVRJMU1URXlPVE13%0D%0AV2pDQmt6RU1NQW9HQTFVRUF4TUQKVlVGTk1SUXdFZ1lEVlFRTEV3dEpiblJsY201aGJDQkpWREVN%0D%0ATUFvR0ExVUVDaE1EVkVOVE1ROHdEUVlEVlFRSEV3Wk5kVzFpWVdreApGREFTQmdOVkJBZ1RDMDFo%0D%0AYUdGeVlYTm9kSEpoTVFzd0NRWURWUVFHRXdKSlRqRXJNQ2tHQ1NxR1NJYjNEUUVKQVJZY2RXeDBh%0D%0AVzFoCmRHbDRjM052TG5OMWNIQnZjblJBZEdOekxtTnZiVENDQWlJd0RRWUpLb1pJaHZjTkFRRUJC%0D%0AUUFEZ2dJUEFEQ0NBZ29DZ2dJQkFOOHEKVWdxTHNSUzI0ZGlTU0pXZ2U1WkR2amFObE9KZmZLOHdt%0D%0ASk5pZVBad3lCY0xTUDJaTkcweGZMV2JJQlFIVjhjT2w3OU9VaDNMUmdtdAp5ZzFSdVhTY0dsTmZu%0D%0AL2VocENKcDU3emJGTkNjRlgrYlFpZjBoTmRYS083UEZZaXN4RTl1VHk1Ym51eW5hU2o4OWMzeDhG%0D%0AV1dscUZ1ClJ4TFM2NmRHVk44OEpSbnM2MVV2WUl3NGIzQld2WUxETkxsUmRWUk5LT0NuVGpaR09F%0D%0ARVlkZTgyR3JXdlBETU9zUnRNdUxWMys4OHAKYVA4N3F0WFdEckV3NlU1VFlGWFE2V3NNSXJnY09j%0D%0ANnJqK2IzSVlvc3pKeHV6N0lhSTRqalZZcSswbjFNMHhaT3NTL3NPcWxlakluOQpOTTFjNWZjRDFw%0D%0AOWpMZFQyY1lKOEd3TzVPZDRmYXJpNDhld1pwMW9BbHVVeVlLTlZCUVVqQ041L3lWbG9zQ0JVVGs4%0D%0AMFZoeHk1bmMxClFDNnlrYWZsVUpUUUYrUWtXcmJwYXg1Y3J4c1RhN0Z0RVNQWXZFcVU1RS9nRS9W%0D%0AeEFnaWVhQ3BkQnN0MTMxWjZiUGo4YW9vbUZ0bFcKanFlaWJldTNiMDdRNE14VEJjek9SRG9kcFNv%0D%0AdFJBbUF5YmlFdmJiUE50a2tXUE5HMm9FT09GVDhhc1JTY1NTVzFCb01rTGl3ek10Uwp2bmxhdHNO%0D%0AcVFkYTRHS2dQdjZOQ01CcVpIK0hLZXg3MFZ5ZEE2aTVRYWRzb1QwTWs4UGxxTEFlSlI1YWhiaUIr%0D%0AQWpaLzJicmExYWRKCjVPTWVaRXJqZFk3cGk5amROU2Vpa1VmeHhMQXpkOFF4KytaSnhoYWFCajZk%0D%0AZVBxUTdjaGV1akMxcGY4WGIrMndNNnlITDI5SEFnTUIKQUFHamdnRlBNSUlCU3pBSkJnTlZIUk1F%0D%0AQWpBQU1CMEdBMVVkRGdRV0JCU09kQ1lzb3ZsNjNFZTFwQ0txeitqUUhNbDJpakNCeUFZRApWUjBq%0D%0AQklIQU1JRzlnQlNoYk51TE5aWUxNWmc4eFNFeXdCVWRZMGZxM3FHQm1hU0JsakNCa3pFTE1Ba0dB%0D%0AMVVFQmhNQ1NVNHhGREFTCkJnTlZCQWdUQzAxaGFHRnlZWE5vZEhKaE1Rd3dDZ1lEVlFRS0V3TlVR%0D%0AMU14RWpBUUJnTlZCQXNUQ1ZWc2RHbHRZWFJwZURFUU1BNEcKQTFVRUF4TUhWRU5USUVOSlR6RXBN%0D%0AQ2NHQ1NxR1NJYjNEUUVKQVJZYVZXeDBhVzFoZEdsNExraGxiSEJrWlhOclFIUmpjeTVqYjIweApE%0D%0AekFOQmdOVkJBY1RCazExYldKaGFZSUpBTXU5T2UxdDlHTFZNRFFHQTFVZEpRUXRNQ3NHQ1dDR1NB%0D%0AR0crRUlFQVFZS0t3WUJCQUdDCk53b0RBd1lJS3dZQkJRVUhBd0VHQ0NzR0FRVUZCd01DTUFzR0Ex%0D%0AVWREd1FFQXdJRm9EQVJCZ2xnaGtnQmh2aENBUUVFQkFNQ0JzQXcKRFFZSktvWklodmNOQVFFTEJR%0D%0AQURnZ0VCQURrM0dJNTNxWG1ZVTBsWHB0b3BVdEE0cUZNQXd1NlZwWVVrVjlKTHUyWExZckdmVjl3%0D%0AaQpmcHlaU1hXcGdhemIrdVFYM1lXL2FRWXZWRXNCT1pPdEMzYk5nakdCdnlhd1lRWkdvaGtsNlpC%0D%0AV2l3VzByQWs5U2Njd2xFbTBXUURMCmVVVnQzUUduZFFLLzZ0R25RMk5YQVdJOW5PT2RWWEpETjAr%0D%0AdlphOVYyK2ljTnZLeDBhN3NhVFFnVXZnSlJMNWQrVGN2VnFheVR2RmIKTGJIWExTSzB6TFYzVUcw%0D%0AeC9OTjFPYmFUL0U0ZXVOUU5jeTE3dFJLRjRZUllkSHB4cmh6ZEsvR3hYUUZoMjhhSi9uQkpIUU55%0D%0ASHR1UApkQUl6cURnNUs3cnRzZy9aTkFmRXlTdWFZcFUvT2VrWURFcjRKTlVaZ2hhYjJxdWxhNXhu%0D%0Aa01JMlhZcENOSjg9CjwvZHM6WDUwOUNlcnRpZmljYXRlPgo8L2RzOlg1MDlEYXRhPgo8L2RzOktl%0D%0AeUluZm8%2BCjwvZHM6U2lnbmF0dXJlPgogICAgPFN0YXR1cz4KICAgICAgICA8U3RhdHVzQ29kZSBW%0D%0AYWx1ZT0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnN0YXR1czpTdWNjZXNzIi8%2BCiAgICA8%0D%0AL1N0YXR1cz4KICAgIDxuczI6QXNzZXJ0aW9uIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRj%0D%0AOlNBTUw6Mi4wOmFzc2VydGlvbiIgSUQ9Il9mNTg1ODE4MGI0N2I0Y2M2ZmNjMDA0ZGJlMGExNmMx%0D%0AOWQ3YTQiIElzc3VlSW5zdGFudD0iMjAxOC0wNi0wMVQyMTowOTowNVoiIFZlcnNpb249IjIuMCI%2B%0D%0ACiAgICAgICAgPG5zMjpJc3N1ZXIgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6%0D%0AbmFtZWlkLWZvcm1hdDplbnRpdHkiPmh0dHBzOi8vd3d3LnVsdGltYXRpeC5uZXQ8L25zMjpJc3N1%0D%0AZXI%2BPGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxk%0D%0Ac2lnIyI%2BCjxkczpTaWduZWRJbmZvPgo8ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0%0D%0AaG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPgo8ZHM6U2lnbmF0%0D%0AdXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxkc2lnLW1v%0D%0AcmUjcnNhLXNoYTI1NiIvPgo8ZHM6UmVmZXJlbmNlIFVSST0iI19mNTg1ODE4MGI0N2I0Y2M2ZmNj%0D%0AMDA0ZGJlMGExNmMxOWQ3YTQiPgo8ZHM6VHJhbnNmb3Jtcz4KPGRzOlRyYW5zZm9ybSBBbGdvcml0%0D%0AaG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJl%0D%0AIi8%2BCjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3ht%0D%0AbC1leGMtYzE0biMiLz4KPC9kczpUcmFuc2Zvcm1zPgo8ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRo%0D%0AbT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjc2hhMjU2Ii8%2BCjxkczpEaWdlc3RW%0D%0AYWx1ZT5DNWhzN1EybUxwT1BVc1JZQkVYT3JIYmNuci92Z3RWMTZJTTVGN3pOYmc4PTwvZHM6RGln%0D%0AZXN0VmFsdWU%2BCjwvZHM6UmVmZXJlbmNlPgo8L2RzOlNpZ25lZEluZm8%2BCjxkczpTaWduYXR1cmVW%0D%0AYWx1ZT4KQVRlNHExOE9nbGsvY0FsbDhhTW9DeHpoWTcwZURhK1BJTzB2Z0dhMTlWV1VuMHdsTlFn%0D%0AUmMySUEzWUQwTGkzeEJPMWVCSzYvelRlMwo5UnB2cjMwcDZPZ2dueGthaEZDdnUrLzEzZ09INys0%0D%0AUjY5NGpFU1V2aWlhWksyVVVWbmo2eWltR0dQZVdSWWVuc1JTclZrYTN3U2p5CmNmZ01PR3M5K2lh%0D%0ARmVHWTV3bEFFUEhaQUZYOFE5cjU3aU92OHJXbDhTVWplcDl4MG9OSDdOVUxnemVnTzIzeWNuWTlI%0D%0AcXRpNk1aVEgKRnRSa2lKdkRYQ1Q2MXJiR0V1T1ZVMllBOXBiY1VHS29Ec3lnT0Jwb2RzaTUwQW1U%0D%0AM3laeEpFdVk3NWRMRlIrRTdHRjUvMUNQZlpCVQpwTnNNaVU1Um1mOE54WTRVVzM4MEJCR1g3RjlE%0D%0AMGZoSFViemlpS3JNYUNpRzFvbG1SZTloTFRDbW0yeExwTHJwVy9oNE1MMUhRS1hoCjdWVGhacml6%0D%0Ac1VNdjF6MVNnTVliMzIxdnlkNG4xVzlRWVFxRHpjOTVla3JzMGhkM2lORms0YUZpamdMRTZlWGlJ%0D%0AKytBWjRRK2c1c2EKT0t3SDRhUHRFdmUxTzhlWmRaUDlIeTZ5SjFieE50blF6UmVEbG9sZS8xZi8x%0D%0Aa3AvN1BuZUxHYU9sZjBYdHpnSVdoSjM2Qms0S1NnWQp6TTFFdmlPQkh5UnJaUUdMRThhSEdGbHhj%0D%0AclNaY01YTU8xOG1oakw2OWIwUy9hRWpkL2FpOFlVUC9LcUtQSFBOeDZsZjArUEkwWXNOCkh1cnJW%0D%0Ac1BoUmRrTlN1YzRGR2d4dlpHenFWQmNiaEhZNnErWHVDeTNFY0VwcWs0bWdhMnQ0eW4xenBJdXkz%0D%0AbXhXa1NaQTQ2bWN2Zz0KPC9kczpTaWduYXR1cmVWYWx1ZT4KPGRzOktleUluZm8%2BCjxkczpYNTA5%0D%0ARGF0YT4KPGRzOlg1MDlDZXJ0aWZpY2F0ZT4KTUlJRjlUQ0NCTjJnQXdJQkFnSUNFSG93RFFZSktv%0D%0AWklodmNOQVFFTEJRQXdnWk14Q3pBSkJnTlZCQVlUQWtsT01SUXdFZ1lEVlFRSQpFd3ROWVdoaGNt%0D%0ARnphSFJ5WVRFTU1Bb0dBMVVFQ2hNRFZFTlRNUkl3RUFZRFZRUUxFd2xWYkhScGJXRjBhWGd4RURB%0D%0AT0JnTlZCQU1UCkIxUkRVeUJEU1U4eEtUQW5CZ2txaGtpRzl3MEJDUUVXR2xWc2RHbHRZWFJwZUM1%0D%0ASVpXeHdaR1Z6YTBCMFkzTXVZMjl0TVE4d0RRWUQKVlFRSEV3Wk5kVzFpWVdrd0hoY05NVFF4TVRJ%0D%0ANE1URXlPVE13V2hjTk1qUXhNVEkxTVRFeU9UTXdXakNCa3pFTU1Bb0dBMVVFQXhNRApWVUZOTVJR%0D%0Ad0VnWURWUVFMRXd0SmJuUmxjbTVoYkNCSlZERU1NQW9HQTFVRUNoTURWRU5UTVE4d0RRWURWUVFI%0D%0ARXdaTmRXMWlZV2t4CkZEQVNCZ05WQkFnVEMwMWhhR0Z5WVhOb2RISmhNUXN3Q1FZRFZRUUdFd0pK%0D%0AVGpFck1Da0dDU3FHU0liM0RRRUpBUlljZFd4MGFXMWgKZEdsNGMzTnZMbk4xY0hCdmNuUkFkR056%0D%0ATG1OdmJUQ0NBaUl3RFFZSktvWklodmNOQVFFQkJRQURnZ0lQQURDQ0Fnb0NnZ0lCQU44cQpVZ3FM%0D%0Ac1JTMjRkaVNTSldnZTVaRHZqYU5sT0pmZks4d21KTmllUFp3eUJjTFNQMlpORzB4ZkxXYklCUUhW%0D%0AOGNPbDc5T1VoM0xSZ210CnlnMVJ1WFNjR2xOZm4vZWhwQ0pwNTd6YkZOQ2NGWCtiUWlmMGhOZFhL%0D%0ATzdQRllpc3hFOXVUeTVibnV5bmFTajg5YzN4OEZXV2xxRnUKUnhMUzY2ZEdWTjg4SlJuczYxVXZZ%0D%0ASXc0YjNCV3ZZTEROTGxSZFZSTktPQ25UalpHT0VFWWRlODJHcld2UERNT3NSdE11TFYzKzg4cAph%0D%0AUDg3cXRYV0RyRXc2VTVUWUZYUTZXc01JcmdjT2M2cmorYjNJWW9zekp4dXo3SWFJNGpqVllxKzBu%0D%0AMU0weFpPc1Mvc09xbGVqSW45Ck5NMWM1ZmNEMXA5akxkVDJjWUo4R3dPNU9kNGZhcmk0OGV3WnAx%0D%0Ab0FsdVV5WUtOVkJRVWpDTjUveVZsb3NDQlVUazgwVmh4eTVuYzEKUUM2eWthZmxVSlRRRitRa1dy%0D%0AYnBheDVjcnhzVGE3RnRFU1BZdkVxVTVFL2dFL1Z4QWdpZWFDcGRCc3QxMzFaNmJQajhhb29tRnRs%0D%0AVwpqcWVpYmV1M2IwN1E0TXhUQmN6T1JEb2RwU290UkFtQXliaUV2YmJQTnRra1dQTkcyb0VPT0ZU%0D%0AOGFzUlNjU1NXMUJvTWtMaXd6TXRTCnZubGF0c05xUWRhNEdLZ1B2Nk5DTUJxWkgrSEtleDcwVnlk%0D%0AQTZpNVFhZHNvVDBNazhQbHFMQWVKUjVhaGJpQitBalovMmJyYTFhZEoKNU9NZVpFcmpkWTdwaTlq%0D%0AZE5TZWlrVWZ4eExBemQ4UXgrK1pKeGhhYUJqNmRlUHFRN2NoZXVqQzFwZjhYYisyd002eUhMMjlI%0D%0AQWdNQgpBQUdqZ2dGUE1JSUJTekFKQmdOVkhSTUVBakFBTUIwR0ExVWREZ1FXQkJTT2RDWXNvdmw2%0D%0AM0VlMXBDS3F6K2pRSE1sMmlqQ0J5QVlEClZSMGpCSUhBTUlHOWdCU2hiTnVMTlpZTE1aZzh4U0V5%0D%0Ad0JVZFkwZnEzcUdCbWFTQmxqQ0JrekVMTUFrR0ExVUVCaE1DU1U0eEZEQVMKQmdOVkJBZ1RDMDFo%0D%0AYUdGeVlYTm9kSEpoTVF3d0NnWURWUVFLRXdOVVExTXhFakFRQmdOVkJBc1RDVlZzZEdsdFlYUnBl%0D%0AREVRTUE0RwpBMVVFQXhNSFZFTlRJRU5KVHpFcE1DY0dDU3FHU0liM0RRRUpBUllhVld4MGFXMWhk%0D%0AR2w0TGtobGJIQmtaWE5yUUhSamN5NWpiMjB4CkR6QU5CZ05WQkFjVEJrMTFiV0poYVlJSkFNdTlP%0D%0AZTF0OUdMVk1EUUdBMVVkSlFRdE1Dc0dDV0NHU0FHRytFSUVBUVlLS3dZQkJBR0MKTndvREF3WUlL%0D%0Ad1lCQlFVSEF3RUdDQ3NHQVFVRkJ3TUNNQXNHQTFVZER3UUVBd0lGb0RBUkJnbGdoa2dCaHZoQ0FR%0D%0ARUVCQU1DQnNBdwpEUVlKS29aSWh2Y05BUUVMQlFBRGdnRUJBRGszR0k1M3FYbVlVMGxYcHRvcFV0%0D%0AQTRxRk1Bd3U2VnBZVWtWOUpMdTJYTFlyR2ZWOXdpCmZweVpTWFdwZ2F6Yit1UVgzWVcvYVFZdlZF%0D%0Ac0JPWk90QzNiTmdqR0J2eWF3WVFaR29oa2w2WkJXaXdXMHJBazlTY2N3bEVtMFdRREwKZVVWdDNR%0D%0AR25kUUsvNnRHblEyTlhBV0k5bk9PZFZYSkROMCt2WmE5VjIraWNOdkt4MGE3c2FUUWdVdmdKUkw1%0D%0AZCtUY3ZWcWF5VHZGYgpMYkhYTFNLMHpMVjNVRzB4L05OMU9iYVQvRTRldU5RTmN5MTd0UktGNFlS%0D%0AWWRIcHhyaHpkSy9HeFhRRmgyOGFKL25CSkhRTnlIdHVQCmRBSXpxRGc1SzdydHNnL1pOQWZFeVN1%0D%0AYVlwVS9PZWtZREVyNEpOVVpnaGFiMnF1bGE1eG5rTUkyWFlwQ05KOD0KPC9kczpYNTA5Q2VydGlm%0D%0AaWNhdGU%2BCjwvZHM6WDUwOURhdGE%2BCjwvZHM6S2V5SW5mbz4KPC9kczpTaWduYXR1cmU%2BCiAgICAg%0D%0AICAgPG5zMjpTdWJqZWN0PgogICAgICAgICAgICA8bnMyOk5hbWVJRCBGb3JtYXQ9InVybjpvYXNp%0D%0AczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OmVtYWlsQWRkcmVzcyI%2BMTM1OTY5Nzwv%0D%0AbnMyOk5hbWVJRD4KICAgICAgICAgICAgPG5zMjpTdWJqZWN0Q29uZmlybWF0aW9uIE1ldGhvZD0i%0D%0AdXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmNtOmJlYXJlciI%2BCiAgICAgICAgICAgICAgICA8%0D%0AbnMyOlN1YmplY3RDb25maXJtYXRpb25EYXRhIEluUmVzcG9uc2VUbz0iX2ZjOTAzNTdkLWIxOTMt%0D%0ANDRkOC04NjIyLWE5ZTIyMWJkYTIxNSIgTm90T25PckFmdGVyPSIyMDE4LTA2LTAxVDIxOjE1OjA1%0D%0AWiIgUmVjaXBpZW50PSJodHRwczovL3BsYXktYXBpLmZyZXNjby5tZS91c2Vycy9hdXRoL3NhbWwv%0D%0AY2FsbGJhY2siLz4KICAgICAgICAgICAgPC9uczI6U3ViamVjdENvbmZpcm1hdGlvbj4KICAgICAg%0D%0AICA8L25zMjpTdWJqZWN0PgogICAgICAgIDxuczI6Q29uZGl0aW9ucyBOb3RCZWZvcmU9IjIwMTgt%0D%0AMDYtMDFUMjE6MDg6MDVaIiBOb3RPbk9yQWZ0ZXI9IjIwMTgtMDYtMDFUMjE6MTU6MDVaIj4KICAg%0D%0AICAgICAgICAgPG5zMjpBdWRpZW5jZVJlc3RyaWN0aW9uPgogICAgICAgICAgICAgICAgPG5zMjpB%0D%0AdWRpZW5jZT5odHRwczovL3BsYXktYXBpLmZyZXNjby5tZS88L25zMjpBdWRpZW5jZT4KICAgICAg%0D%0AICAgICAgPC9uczI6QXVkaWVuY2VSZXN0cmljdGlvbj4KICAgICAgICA8L25zMjpDb25kaXRpb25z%0D%0APgogICAgICAgIDxuczI6QXV0aG5TdGF0ZW1lbnQgQXV0aG5JbnN0YW50PSIyMDE4LTA2LTAxVDIx%0D%0AOjA5OjA0WiIgU2Vzc2lvbkluZGV4PSI0ZVRNcTJNRzlVNmNHdkkvbmt2Uk94djVEUzQ9VUZyaXVB%0D%0APT0iIFNlc3Npb25Ob3RPbk9yQWZ0ZXI9IjIwMTgtMDYtMDFUMjE6MTU6MDVaIj4KICAgICAgICAg%0D%0AICAgPG5zMjpBdXRobkNvbnRleHQ%2BCiAgICAgICAgICAgICAgICA8bnMyOkF1dGhuQ29udGV4dENs%0D%0AYXNzUmVmPnVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphYzpjbGFzc2VzOlBhc3N3b3JkPC9u%0D%0AczI6QXV0aG5Db250ZXh0Q2xhc3NSZWY%2BCiAgICAgICAgICAgIDwvbnMyOkF1dGhuQ29udGV4dD4K%0D%0AICAgICAgICA8L25zMjpBdXRoblN0YXRlbWVudD4KICAgICAgICA8bnMyOkF0dHJpYnV0ZVN0YXRl%0D%0AbWVudD4KICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0iZW1haWwiIE5hbWVGb3JtYXQ9%0D%0AInVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6YmFzaWMiPgogICAg%0D%0AICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGVWYWx1ZT5tYXJjby5jYWJyZWpvc0B0Y3MuY29tPC9u%0D%0AczI6QXR0cmlidXRlVmFsdWU%2BCiAgICAgICAgICAgIDwvbnMyOkF0dHJpYnV0ZT4KICAgICAgICAg%0D%0AICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0ibGFzdF9uYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6%0D%0AbmFtZXM6dGM6U0FNTDoyLjA6YXR0cm5hbWUtZm9ybWF0OmJhc2ljIj4KICAgICAgICAgICAgICAg%0D%0AIDxuczI6QXR0cmlidXRlVmFsdWU%2BQ2FicmVqb3MgTWFsbWE8L25zMjpBdHRyaWJ1dGVWYWx1ZT4K%0D%0AICAgICAgICAgICAgPC9uczI6QXR0cmlidXRlPgogICAgICAgICAgICA8bnMyOkF0dHJpYnV0ZSBO%0D%0AYW1lPSJmaXJzdF9uYW1lIiBOYW1lRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6%0D%0AYXR0cm5hbWUtZm9ybWF0OmJhc2ljIj4KICAgICAgICAgICAgICAgIDxuczI6QXR0cmlidXRlVmFs%0D%0AdWU%2BTWFyY29hbnRvbmlvPC9uczI6QXR0cmlidXRlVmFsdWU%2BCiAgICAgICAgICAgIDwvbnMyOkF0%0D%0AdHJpYnV0ZT4KICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGUgTmFtZT0iZW1wbG95ZWVfaWQiIE5h%0D%0AbWVGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphdHRybmFtZS1mb3JtYXQ6YmFz%0D%0AaWMiPgogICAgICAgICAgICAgICAgPG5zMjpBdHRyaWJ1dGVWYWx1ZT4xMzU5Njk3PC9uczI6QXR0%0D%0AcmlidXRlVmFsdWU%2BCiAgICAgICAgICAgIDwvbnMyOkF0dHJpYnV0ZT4KICAgICAgICA8L25zMjpB%0D%0AdHRyaWJ1dGVTdGF0ZW1lbnQ%2BCiAgICA8L25zMjpBc3NlcnRpb24%2BCjwvUmVzcG9uc2U%2B";
            var contentParam = new Dictionary<String, String>();
            contentParam.Add("SAMLResponse", getSAMLResponse(responseData));
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);

            try
            {
                var cookieContainer = new CookieContainer();
                var handler = new HttpClientHandler() { CookieContainer = cookieContainer };

                using (HttpClient client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    client.DefaultRequestHeaders.Add("Host", "play-api.fresco.me");
                    client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                    client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
                    client.DefaultRequestHeaders.Add("Accept-Language", "es-ES,es;q=0.9,en;q=0.8");
                    client.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                    client.DefaultRequestHeaders.Add("Origin", "https://authapp.ultimatix.net");
                    client.DefaultRequestHeaders.Connection.Add("keep-alive");
                    client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                    client.DefaultRequestHeaders.Add("Referer", RequestURL);
                    client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    // client.DefaultRequestHeaders.Add("Cookie", newCookie);
                    //HttpRequestMessage request = new 

                    //var req = new HttpRequestMessage(HttpMethod.Post, baseUrl+requestUrl) { Content = new FormUrlEncodedContent(contentParam) };
                    //var res = await client.SendAsync(req);


                    using (HttpResponseMessage response = await client.PostAsync(requestUrl, new FormUrlEncodedContent(contentParam)))
                    {
                        RequestURL = response.RequestMessage.RequestUri.ToString();
                        Console.WriteLine(RequestURL);
                        using (HttpContent content = response.Content)
                        {
                            //responseData = await content.ReadAsStringAsync();
                            Cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex) { }

            //await GetApiKey(NextRequestURL);
        }

        public async Task GetApiKey()
        {
            String baseUrl = "https://play-api.fresco.me/";
            String requestUrl = "saml_signin.json";

            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);

            var queryContent = new FormUrlEncodedContent(new KeyValuePair<string, string>[]{
                new KeyValuePair<string, string>("code",RequestURL.Substring(RequestURL.IndexOf("=")).TrimStart('='))
            });

            String responseData = "";

            try
            {
                var cookieContainer = new CookieContainer();
                var handler = new HttpClientHandler() { CookieContainer = cookieContainer/*, Proxy = GetProxy()*/ };

                using (HttpClient client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
                    //client.DefaultRequestHeaders.Add("Host", "play-api.fresco.me");
                    //client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
                    //client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
                    //client.DefaultRequestHeaders.Add("Accept-Language", "es-ES,es;q=0.9,en;q=0.8");
                    //client.DefaultRequestHeaders.Add("Cache-Control", "max-age=0");
                    //client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                    ////client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Length", "14497");
                    //client.DefaultRequestHeaders.Add("Origin", "https://authapp.ultimatix.net");
                    //client.DefaultRequestHeaders.Connection.Add("keep-alive");
                    //client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                    //client.DefaultRequestHeaders.Add("Referer", NextRequestURL);
                    //client.DefaultRequestHeaders.Add("Upgrade-Insecure-Requests", "1");
                    // client.DefaultRequestHeaders.Add("Cookie", newCookie);
                    //HttpRequestMessage request = new 

                    //var req = new HttpRequestMessage(HttpMethod.Post, baseUrl+requestUrl) { Content = new FormUrlEncodedContent(contentParam) };
                    //var res = await client.SendAsync(req);


                    using (HttpResponseMessage response = await client.PostAsync(requestUrl, queryContent))
                    {
                        //NextRequestURL = response.RequestMessage.RequestUri.ToString();
                        //Console.WriteLine(NextRequestURL);
                        using (HttpContent content = response.Content)
                        {
                            responseData = await content.ReadAsStringAsync();
                        }
                    }
                }
            }
            catch (Exception ex) { }

        }

        public WebProxy GetProxy()
        {
            var proxy = new WebProxy()
            {
                Address = new Uri($"http://proxy.financiero.bco:8080"),
                UseDefaultCredentials = false,
                // *** These creds are given to the proxy server, not the web server ***
                Credentials = new NetworkCredential(userName: "MARCAJ", password: "Macuton!0$")
            };
            return proxy;
        }
    }
}